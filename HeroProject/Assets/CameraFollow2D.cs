﻿using UnityEngine;
using System.Collections;

public class CameraFollow2D : MonoBehaviour
{

	// Use this for initialization
	void Start () {
	
	}

    private Vector3 velocity = Vector3.zero;
    public Transform target;
    float orthoSize;
    public float recoveryRate = 100;
    public float speedToZoom = 0;
	// Update is called once per frame
	void FixedUpdate () {
        if (target)
        {
            Vector3 point = GetComponent<Camera>().WorldToViewportPoint(target.position);
            Vector3 delta = target.position - GetComponent<Camera>().ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
            Vector3 destination = transform.position + delta;
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, 0);
        }

        //Camera zoom on car speed;
        
        float zoomfactor = 0;
            
        zoomfactor = target.GetComponent<Rigidbody2D>().velocity.x;
        zoomfactor = (zoomfactor < 0 ? zoomfactor *= -1 : zoomfactor);

        if (zoomfactor < speedToZoom)
        {
            zoomfactor = 0;
        }
        orthoSize = Mathf.MoveTowards(orthoSize,(float)(10 + (zoomfactor * .5f)), Time.deltaTime * recoveryRate);

        orthoSize = Mathf.Clamp(orthoSize, 10, 13);
        //Debug.Log("Car Speed" + zoomfactor);

        GetComponent<Camera>().orthographicSize =  orthoSize;

	}
}
