﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]

public class HeroController : MonoBehaviour {

    public float jumpHeight = 4;
    public float timeToJumpApex = .4f;
    float accelerationTimeAirborne = .2f;
    float accelerationTimeGrounded = .1f;
    float moveSpeed = 6;
    float moveCrouchedSpeed = 2; 

    float gravity;
    float jumpVelocity;
    Vector3 velocity;
    float velocityXSmoothing;

    public CharacterController myController;
    public Animator myAnimator;
    // Use this for initialization
    void Start () {

        if (myController == null) myController = GetComponent<CharacterController>();

        gravity = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;

        //Hash Values...
        hashJump = Animator.StringToHash("Jump");
        hashHorizontalSpeed = Animator.StringToHash("HorizontalSpeed");
        hashVerticalSpeed = Animator.StringToHash("VerticalSpeed");
       
        hashCrounched = Animator.StringToHash("Crounched");
    }

    public bool isgrounded = false;
    public bool sideHit = false;
    public bool iscrounched = false;
    float rotation = 180;
	// Update is called once per frame
	void Update () {


        isgrounded = (myController.collisionFlags & CollisionFlags.Below) != 0;
        sideHit = (myController.collisionFlags & CollisionFlags.CollidedSides) != 0;

        if (isgrounded !=myController.isGrounded)
        {
            Debug.Break();
        }

        if ((myController.collisionFlags  & CollisionFlags.Above) != 0 || isgrounded)
        {
            velocity.y = 0;
        }

        Vector3 input = new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"), 0);

        iscrounched = Input.GetKey(KeyCode.LeftShift);

        if (Input.GetKeyDown(KeyCode.Space) && isgrounded)
        {
            velocity.y = jumpVelocity;
        }

        float targetVelocityX = input.x * (iscrounched ? moveCrouchedSpeed : moveSpeed);


        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, isgrounded ? accelerationTimeGrounded : accelerationTimeAirborne);

        if (input.x == 0 && isgrounded) velocity.x = 0;

        velocity.y += gravity * Time.deltaTime;
        myController.Move(velocity * Time.deltaTime);

        SetAnimationParameters();

        if (velocity.x > 0.01) rotation = 90;
        else if (velocity.x < -0.01) rotation = 270;
        //else rotation = 180;

        myAnimator.transform.localEulerAngles = new Vector3(myAnimator.transform.localEulerAngles.x, Mathf.MoveTowards(myAnimator.transform.localEulerAngles.y,  rotation, 500.0f * Time.deltaTime), myAnimator.transform.localEulerAngles.z);

       // Debug.Log(myController.velocity.y);
    }

    int hashJump = -1;
    int hashHorizontalSpeed = -1;
    int hashVerticalSpeed = -1;
   
    int hashCrounched = -1;

    float smoothHorizontalVelocity;
    float smoothVerticalVelocity;
    private void SetAnimationParameters()
    {
        myAnimator.SetBool(hashJump, !isgrounded);
        myAnimator.SetBool(hashCrounched, iscrounched);
        //myAnimator.SetBool(hashFall, (velocity.y < - 1.0f)?true:false);

        smoothHorizontalVelocity = Mathf.MoveTowards(smoothHorizontalVelocity, Mathf.Clamp01(Mathf.Abs(velocity.x)), 0.1f);
        if (sideHit) smoothHorizontalVelocity = 0;

        myAnimator.SetFloat(hashHorizontalSpeed, smoothHorizontalVelocity /* Mathf.Clamp01(Mathf.Abs(velocity.x))*/);

        smoothVerticalVelocity = Mathf.MoveTowards(smoothVerticalVelocity, Mathf.Clamp01((velocity.y < -1.0f) ? 1 : 0), 0.1f);
        myAnimator.SetFloat(hashVerticalSpeed, smoothVerticalVelocity /*(velocity.y < -1.0f) ? 1 : 0*/);


    }
}
